/* eslint-disable no-console */
// import axios from 'axios';

'use-strict';

const axios = require('axios');

module.exports = class ApiService {
  url;

  constructor(url) {
    this.url = url;

    this.getUrl = this.getUrl.bind(this);

    this.getDevice = this.getDevice.bind(this);
    this.fill = this.fill.bind(this);
  }

  getUrl() {
    return this.url;
  }

  async getDevice() {
    const infos = `ApiService.getDevice(${this.url})`;
    // console.info(`${new Date()} :: ${infos}`);

    let result = null;

    try {
      const { data, status } = await axios.get(this.url);

      if (status != 200) {
        throw new Error('NetworkError');
      }

      result = data.result;
      // console.info(`${new Date()} :: ${infos} ::`, { result });
    } catch(error) {
      this.handleError(infos, error);
    }
    return result;
  }

  async fill(color) {
    const infos = `ApiService.fill(${this.url}, ${color})`;
    // console.info(`${new Date()} :: ${infos}`);

    let result = false;

    try {
      const { data, status } = await axios.get(`${this.url}/fill/${color}`);

      if (status !== 200) {
        throw new Error('NetworkError');
      }
      if (data.errors.length !== 0) {
        throw new Error(data.errors);
      }

      result = true;
      console.info(`${new Date()} :: ${infos} ::`, result);
    } catch(error) {
      this.handleError(infos, error);
    }
    return result;
  }

  async arrayFill(colors) {
    const infos = `ApiService.arrayFill(${this.url}, ${colors})`;
    console.info(`${new Date()} :: ${infos}`);

    let result = false;

    try {

      const { data, status } = await axios.post(`${this.url}/array_fill`, { colors });

      if (status !== 200) {
        throw new Error('NetworkError');
      }
      if (data.errors.length !== 0) {
        throw new Error(data.errors);
      }

      result = true;
      // console.info(`${new Date()} :: ${infos} ::`, result);
    } catch(error) {
      this.handleError(infos, error);
    }
    return result;
  }

  handleError(infos, error) {
    console.error(`${new Date()} :: ${infos} :: ERROR : `, error.message);
  }
}

// export default ApiService;