// "use strict";

// var { Formats, Characteristic } = require('homebridge');
var ApiService = require('./api.service');
var inherits = require('util').inherits;

// var Service, Characteristic, HomebridgeAPI;

module.exports = function(homebridge) {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  Accessory = homebridge.Accessory;
  HomebridgeAPI = homebridge;

  homebridge.registerAccessory("homebridge-esgiled", "LED", LedAccessory);

  ColorsCharacteristics = function() {
    Characteristic.call(this, 'Colors', ColorsCharacteristics.UUID);

		this.setProps({
	    	format: Characteristic.Formats.STRING,
	    	perms: [Characteristic.Perms.READ, Characteristic.Perms.WRITE, Characteristic.Perms.NOTIFY]
	    });
		this.value = this.getDefaultValue();
	}
	inherits(ColorsCharacteristics, Characteristic);
  ColorsCharacteristics.UUID = '000000CE-0000-1000-8000-0026ABCDEF03';
}

function LedAccessory(log, config) {
  this.log = log;
  this.config = config;
  this.name = config.name;
  this.state = {
    On: false,
    Colors: "[BLANK]",
  }

  if (!config.url || config.url == "")
		throw new Error("Missing or invalid location configuration");

  this.url = config.url;
  this.apiService = new ApiService(this.url);

  this.informationService = new Service.AccessoryInformation();
  this.informationService
    .setCharacteristic(Characteristic.Manufacturer, '5MOC')
    .setCharacteristic(Characteristic.Model, 'Raspberry-LED')
    .setCharacteristic(Characteristic.SerialNumber, 'LED-123');

  this._service = new Service.Lightbulb("LED");
  
  this._onCharacteristic = this._service.getCharacteristic(Characteristic.On);
  this._onCharacteristic.on('set', this.handleSetOn.bind(this));
  this._onCharacteristic.on('get', this.handleGetOn.bind(this));

  this._service.addCharacteristic(ColorsCharacteristics);
  this._colors = this._service.getCharacteristic(ColorsCharacteristics);
  this._colors.on('set', this.handleSetColors.bind(this))
  this._colors.on('get', this.handleGetColors.bind(this));

}

LedAccessory.prototype.getServices = function() {
  return [this.informationService, this._service];
}

LedAccessory.prototype.handleSetOn = async function(on, callback) {
  const response = await this.apiService.fill(on ? "WHITE" : "BLANK");
  const success = response != null && response != undefined;

  if (success){ 
    this.state.On = on;
    this._onCharacteristic.value = this.state.On;
    console.info(`Set Characteristic 'On' to ${this.state.On}, successfull`);
  }
  else {
    console.error(`Set Characteristic 'On' to ${on}, failed`);
  }
  return callback(null);
}

LedAccessory.prototype.handleGetOn = async function(callback) {
  const response = await this.apiService.getDevice();
  const success = response && response.colors;

  if (success) {
    const isOn =  response.colors != 'BLANK';
    this.state.On = isOn;
    this._onCharacteristic.value = this.state.On;
    console.info(`Get Characteristic 'On' = ${this.state.On}`);
  }
  else {
    console.info(`Get Characteristic 'On' failed`);
  }
  return callback(null, this.state.On);
}


LedAccessory.prototype.handleSetColors = async function(values, callback) {
  var colors = values.split(',');

  var response = await this.apiService.arrayFill(colors);
  const success = response != null && response != undefined;

  if (success) {
    this.state.Colors = colors.join(',');
    this._colors.value = this.state.Colors;
    console.info(`Set Characteristic 'Colors' to ${this.state.Colors}, successfull`);
  }
  else {
    console.error(`Set Characteristic 'Colors' to ${values}, failed`);
  }
  return callback(null);
}

LedAccessory.prototype.handleGetColors = async function(callback) {
  var response = await this.apiService.getDevice();
  const success = response != null && response != undefined;

  if (success) {
    const colors =  response.colors;
    this.state.Colors = colors.join(',');
    this._colors.value = this.state.Colors;
    // this.service.setCharacteristic(ColorsCharacteristics, this.state.Colors);

    console.info(`Get Characteristic 'Colors' = ${this.state.Colors}`);
  }
  else{
    console.error(`Get Characteristic 'Colors' failed`);
  }
  return callback(null, this.state.Colors);
}

// LedAccessory.prototype.update = async function() {
//   await this.handleGetOn();
//   await this.handleGetColors();
// }